var express = require('express');
var _ = require('underscore')._;
var fs = require('fs');

var app = express();
var items = [];

// Main

app.get('/', function(req, res) {
	res.writeHead(200, {'content-type':'text/html'});
	res.end(fs.readFileSync('./list.html'));
});

// Get

app.get('/todo', function(req, res){
	if (items.length === 0) {
		res.statusCode = 204;
		res.end('No Content');

		return;
	}

	res.writeHead(200,{'content-type':'application/json'});
	res.end(JSON.stringify(items));
});

// POST

app.post('/todo', function(req, res) {
	var item = '';

	req.setEncoding('utf8');
	req.on('data', function (chunk) {
		item += chunk;
	});

	req.on('end', function () {
		var last = _.last(items);
		var last_id = (last === undefined) ? 0 : parseInt(last.id,10) + 1;

		items.push({id:last_id, todo: item});
		res.end('ok');
	});
});

// DELETE

app.del('/todo/:id', function(req, res) {
	var item_id = parseInt(req.params.id, 10);

	if (isNaN(item_id)) {
		res.statusCode = 400;
		res.end('Invalid');
	}

	else if (!_.findWhere(items, {id:item_id})) {
		res.statusCode = 404;
		res.end('Not Found');
	}

	else {
		var results = _.reject(items, function(item){
			return item.id === item_id;
		});

		items = results;
		res.end('ok');
	}
});

// PUT

app.put('/todo/:id', function(req, res) {
	var item_id = parseInt(req.params.id,10);
	var	item = _.findWhere(items, {id:item_id});

	if (isNaN(item_id)) {
		res.statusCode = 400;
		res.end('Invalid');
	}

	else if (!item) {
		res.statusCode = 404;
		res.end('Not Found');
	}

	else {
		var todo_item = '';

		req.setEncoding('utf8');
		req.on('data', function (chunk) {
			todo_item += chunk;
		});

		req.on('end', function () {
			if (todo_item === item.todo) {
				res.statusCode = 304;
				res.end('No Changes');
			}

			items = _.map(items, function(i) {
				if (i.id === item_id) {
					i.todo = todo_item;
				}

				return i;
			});

			res.end('ok');
		});
	}
});

// Listen

app.listen(9090);

console.log("Server has started.");